# Run & hack OLIP locally!

The current repository allows you to launch a whole OLIP project using docker-compose.

You can start it with minimal access to files and database however the table below shows a few scenarios to get a wider access to OLIP. It uses Docker override files to mount configuration files from host and enable acess to the database, `descriptor.json` file and source code

For further informations about what is OLIP, [check out documentation](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/). For any questions with Docker, you can [check out the short tutorial](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/knowledge-base/debug-an-app/) about debuggin a WebApp, it will help you out with few command lines.

|   |Read only|File access from host|Live development from sources|Build Docker images from sources|
|---:|:---:|:---:|:---:|:---:|
|Docker override to use|`docker-compose.yml`|`docker-compose.file-access.yml`|`docker-compose.live-dev.yml`|`docker-compose.local-build.yml`|
| Docker image in use| Docker Hub | Docker Hub | Local build | Local build |
|app.db|   |✓|✓|✓|
|descriptor.json|   |✓|✓|✓|
|source code|   | |✓|✓|

## Install Docker & Docker Compose

We use Docker compose file to setup OLIP, therefore you need to install [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) software (make sure docker-compose >= 1.25.0).

We use buildx in our docker file and therefore we need to enable build kit for docker-compose, this can be done by adding `COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1` in front of the `docker-compose` command line.
(Note that you need to have BuildKit builds enabled on your system Docker installation. Please refer to the [official docs](https://docs.docker.com/develop/develop-images/build_enhancements/#to-enable-buildkit-builds). On linux that should be as simple as editing `/etc/docker/daemon.json` and setting this feature to `true`: `{ "features": { "buildkit": true } }`)

On Linux you can follow the [Docker post-install](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) to add your user to the Docker group, otherwhise keep in mind that you'll have tu use `sudo` in front of each `docker-compose` command.
## Get the source code

This will download a few Docker compose files and initialize OLIP as git submodule to ease up coding:

```shell
git clone --recurse-submodules https://gitlab.com/bibliosansfrontieres/olip/olip-development.git
cd olip-development
```

(Please note that if you discover that the submodules are not are their latest version, you can update them with `git submodule foreach git pull origin master`.)
## Read only

```shell
docker-compose up
```

Populate OLIP catalog with Apps & Content : [http://localhost:5002/applications/?visible=true&repository_update=true](http://localhost:5002/applications/?visible=true&repository_update=true)

* OLIP Dashboard is available at http://localhost:8080
* OLIP API is available at http://localhost:5002
* Catalog of Apps and Content : Predefined `descriptor.json` fetched from https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/descriptor.json
* SQLite database is stored in `/data/app.db` within the container called `api`
* Content App is located in `./data/olip/my_app.app`

## File access from host

If you want to access the database `app.db` or provide your own descriptor file use

```shell
docker-compose -f docker-compose.yml -f docker-compose.file-access.yml up
```

Populate OLIP catalog with Apps & Content : [http://localhost:5002/applications/?visible=true&repository_update=true](http://localhost:5002/applications/?visible=true&repository_update=true)

* OLIP Dashboard is available at http://localhost:8080
* OLIP API is available at http://localhost:5002
* [Catalog of Apps and Content](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/packaging-tutorial/register-app-catalog/) : available in `./data/descriptor/descriptor.json`
  * update it with the latest version: `wget https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/descriptor.json -O ./data/descriptor/descriptor.json`
* SQLite database is available in `./data/olip/app.db`
* Content App is located in `./data/olip/my_app.app`

## Live development from sources

This will help you setup a live environment to develop OLIP.

OLIP is ran from source code available in these git submodules:
* `./olip-api`
* `./olip-dashboard`

```shell
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.yml -f docker-compose.file-access.yml -f docker-compose.live-dev.yml build
docker-compose -f docker-compose.yml -f docker-compose.file-access.yml -f docker-compose.live-dev.yml up
```

Populate OLIP catalog with Apps & Content : [http://localhost:5002/applications/?visible=true&repository_update=true](http://localhost:5002/applications/?visible=true&repository_update=true)

* OLIP Dashboard is available at http://localhost:8080 (you may need to wait a minute while the development server launches inside the container)
* OLIP API is available at http://localhost:5002
* [Catalog of Apps and Content](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/packaging-tutorial/register-app-catalog/) : available in `./data/descriptor/descriptor.json`
  * update it with the latest version: `wget https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/descriptor.json -O ./data/descriptor/descriptor.json`
* SQLite database is available in `./data/olip/app.db`
* Content App is located in `./data/olip/my_app.app`

## Build Docker images from sources

Use this command line if you want to build Docker images from your local source code and execute it:

```shell
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.yml -f docker-compose.file-access.yml -f docker-compose.local-build.yml build
docker-compose -f docker-compose.yml -f docker-compose.file-access.yml -f docker-compose.local-build.yml up
```

Populate OLIP catalog with Apps & Content : [http://localhost:5002/applications/?visible=true&repository_update=true](http://localhost:5002/applications/?visible=true&repository_update=true)

* OLIP Dashboard is available at http://localhost:8080
* OLIP API is available at http://localhost:5002
* [Catalog of Apps and Content](http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/olip/packaging-tutorial/register-app-catalog/) : available in `./data/descriptor/descriptor.json`
  * update it with the latest version: `wget https://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/descriptor.json -O ./data/descriptor/descriptor.json`
* SQLite database is available in `./data/olip/app.db`
* Content App is located in `./data/olip/my_app.app`

## EXTRA : Activate a reverse proxy to build statistics

We recently setup a reverse proxy to build statistics, you can try this feature by using the `docker-compose.with-proxy.yml`  Docker override:

```shell
docker-compose -f docker-compose.yml -f docker-compose.file-access.yml -f docker-compose.with-proxy.yml up
```

* OLIP Dashboard is available at http://localhost
* OLIP API is available at http://api.localhost
* Apps are now available on a sub domain such as http://app.localhost
* Stats are avaible at http://stats.localhost (admin/admin)
