// report.js
// This fils overloads the generated report of goaccess
(function(){
	// Build the links to the 4 reports
	var reports = [
		{link: "index.html", label: "Report"},
		{link: "report_day.html", label: "Daily Report"},
		{link: "report_week.html", label: "Weekly Report"},
		{link: "report_month.html", label: "Monthly Report"}
	];
	var oTarget = document.getElementsByClassName("label label-info")[0].parentNode;
	var sCurrentPage = '', sNewContent = '', sContent = oTarget.innerHTML;
	for (var n=0; n < reports.length; n ++){
		sNewContent += '<button class="btn btn-default btn-sm" style="margin: 3px;"><a href="' + reports[n].link + '">';
		sNewContent += '<i class="fa fa-external-link"></i> ' + reports[n].label + ' </a> </button>';
		if (window.location.href.indexOf(reports[n].link) > 0){
			sCurrentPage = '(' + reports[n].label + ')';
		}
	}
	oTarget.innerHTML = sNewContent + sContent;
		
	// Change the title of the page according to its url
	document.getElementsByTagName('h1')[0].children[0].innerHTML = '<i class="fa fa-tachometer"></i> OLIP dashboard ' + sCurrentPage;
})();