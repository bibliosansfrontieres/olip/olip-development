#!/bin/bash

# for testing purposes, override the working dir with an environment variable
DOCUMENTROOT=${DOCUMENTROOT:-"/var/www/olip.ideascube.org"}
ARCH="amd64 arm32v7 i386"
DOCKERTAGS="testing staging"

for thisarch in $ARCH ; do

    mkdir -p "${DOCUMENTROOT}/${thisarch}" || {
        >&2 echo "Error: Destination path doesn't exist: $DOCUMENTROOT"
        exit 1
    }

    for thistag in $DOCKERTAGS ; do

        srcfile="${DOCUMENTROOT}/${thisarch}/descriptor.json"
        dstfile="${DOCUMENTROOT}/${thisarch}/descriptor-${thistag}.json"

        rm "${dstfile}"
        sed "/image.*:latest/s/latest/${thistag}/" "$srcfile" > "$dstfile"
        touch "${dstfile}" --reference="${srcfile}"
        chmod a+r "${dstfile}"

    done

done
