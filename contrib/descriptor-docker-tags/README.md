# descriptor-docker-tags

This script takes the original `descriptor.json` file and replaces `:latest` docker tags by `:staging` and `:testing` tags to the corresponding files.

This allow to link an OLIP instance to "unstable" images so you can debug, troubleshoot, test while developping an OLIP app.

## How to use

This script is ran nightly from a cronjob:

```text
17 3    *   *   *   /usr/local/bin/descriptor-docker-tags.bash
```

For testing purpose, you can override the `$DOCUMENTROOT` workdir:

```shell
DOCUMENTROOT=$(pwd) ./descriptor-docker-tags.bash
```

In this example, you need to create the source files first:

```shell
for i in amd64 arm32v7 i386 ; do mkdir -p $i ; wget -q http://olip.bibliosansfrontieres.org/${i}/descriptor.json -O ${i}/descriptor.json ; done
```

## Warning

Because the `descriptor.json` URL has to point to a **folder** - and not the file itself, in theory we can not point OLIP to these `-suffix` files. See [olip-api #34](https://gitlab.com/bibliosansfrontieres/olip/olip-api/-/issues/34).

As a workaround, some rewrite directives are to be added to your nginx vhost:

```
rewrite ^/(.*)/descriptor-(.*).json/descriptor.json http://olip.ideascube.org/$1/descriptor-$2.json permanent;
rewrite ^/(.*)/descriptor-(.*).json/(.*).png http://olip.ideascube.org/$1/$3.png permanent;
```

